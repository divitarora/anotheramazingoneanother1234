package com.maf.AnotherAmazingOneAnother;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotherAmazingOneAnother1234Application {

	public static void main(String[] args) {
		SpringApplication.run(AnotherAmazingOneAnother1234Application.class, args);
	}

}
